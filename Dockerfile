FROM lainventoria/sdk-ruby:latest as build
ENV PACKAGER "fauno <fauno@lainventoria.com.ar>"
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1
ENV SECRET_KEY_BASE solo_es_necesaria_para_correr_rake
ENV RAILS_ENV production
RUN gem install --no-document bundler:2.0.2

# find | xargs es más rápido que un chown recursivo
USER app
WORKDIR /home/app/web
COPY --chown=app:www-data ./Gemfile .
COPY --chown=app:www-data ./Gemfile.lock .
# Instalar las gemas de producción
RUN bundle install --path=./vendor --without test development
# Eliminar cosas que ya no sirven
RUN rm vendor/ruby/2.5.0/cache/*.gem
# Limpiar las librerías nativas
RUN find vendor -name "*.so" | xargs -rn 1 strip --strip-unneeded
COPY --chown=app:www-data ./.git/ ./.git/
# Hacer un tarball de los archivos de la web
RUN git archive -o ../web.tar.gz HEAD
COPY --chown=app:www-data ./app/assets ./app/assets
# Pre-compilar los assets
COPY --chown=app:www-data ./Rakefile .
COPY --chown=app:www-data ./config/ ./config/
COPY --chown=app:www-data ./app/ ./app/
COPY --chown=app:www-data ./yarn.lock .
COPY --chown=app:www-data ./package.json .
RUN yarn
RUN bundle exec rake assets:precompile
# Eliminar la necesidad de un runtime JS en producción, porque los
# assets ya están pre-compilados
RUN sed -re "/(uglifier|bootstrap|coffee-rails)/d" -i Gemfile
RUN bundle clean

FROM lainventoria/monit:latest
RUN apk add --no-cache libxslt libxml2 tzdata ruby ruby-bundler ruby-json ruby-bigdecimal ruby-rake
RUN apk add --no-cache postgresql-libs
RUN apk add --no-cache imagemagick file
# Agregar el usuario y grupo
RUN addgroup -g 82 -S www-data
RUN apk add --no-cache darkhttpd
COPY ./entrypoint.sh /usr/bin/entrypoint
RUN chmod 755 /usr/bin/entrypoint
# Agregar el usuario
RUN adduser -s /bin/sh -G www-data -h /srv/http -D app
# Traer los archivos de la web y colocarlos donde van definitivamente
USER app
COPY --from=build --chown=app:www-data /home/app/web.tar.gz /tmp/
WORKDIR /srv/http
RUN tar xf /tmp/web.tar.gz && rm /tmp/web.tar.gz
# Traer los assets compilados
COPY --from=build --chown=app:www-data /home/app/web/public/assets public/assets
# Traer las gemas
COPY --from=build --chown=app:www-data /home/app/web/vendor ./vendor
COPY --from=build --chown=app:www-data /home/app/web/.bundle ./.bundle
# Y el .env
COPY --chown=app:www-data ./.env ./.env
COPY --from=build --chown=app:www-data /home/app/web/Gemfile ./Gemfile
COPY --from=build --chown=app:www-data /home/app/web/Gemfile.lock ./Gemfile.lock

USER root
RUN gem install --no-document bundler:2.0.2
RUN install -m 640 -o root -g root ./monit.conf /etc/monit.d/puma.conf
RUN monit -t
VOLUME "/srv/http/public/system"
EXPOSE 3000
