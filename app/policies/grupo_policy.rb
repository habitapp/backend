class GrupoPolicy < ApplicationPolicy
  attr_reader :administradora, :grupo

  def initialize(administradora, grupo)
    @administradora = administradora
    @grupo = grupo
  end

  def index?
    true
  end

  def new?
    create?
  end

  def create?
    administradora.super?
  end

  def show?
    true
  end

  def edit?
    update?
  end

  def update?
    true
  end

  def destroy?
    administradora.super?
  end

  class Scope
    attr_reader :administradora, :grupos

    def initialize(administradora, grupos)
      @administradora = administradora
      @grupos = grupos
    end

    # Las administradoras de grupo solo pueden ver sus propios grupos
    def resolve
      if administradora.super?
        grupos.all
      else
        administradora.grupos
      end
    end
  end
end
