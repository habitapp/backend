class ApplicationPolicy
  attr_reader :administradora, :record

  def initialize(administradora, record)
    @administradora = administradora
    @record = record
  end

  def index?
    false
  end

  def show?
    false
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  class Scope
    attr_reader :administradora, :scope

    def initialize(administradora, scope)
      @administradora = administradora
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
