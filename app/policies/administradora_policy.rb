class AdministradoraPolicy < ApplicationPolicy
  attr_reader :administradora, :otra_administradora

  def initialize(administradora, otra_administradora)
    @administradora = administradora
    @otra_administradora = otra_administradora
  end

  def new?
    create?
  end

  def create?
    true
  end

  # Puede convertir otras administradoras en super administradoras?
  def super?
    administradora.super? && !otra_administradora.super?
  end

  # Solo otras super administradoras pueden quitar permisos
  def unsuper?
    administradora.super? && otra_administradora.super?
  end

  # Pueden editar a otras administradoras?
  def edit?
    update?
  end

  # Solo pueden hacer cambios si son super o estan en el mismo grupo
  def update?
    return true if administradora.super?
    return true if administradora.administradoras.include?(otra_administradora) && !otra_administradora.super?

    false
  end

  # Puede bloquear y desbloquear?
  def lock?
    update?
  end

  def unlock?
    update?
  end

  def destroy?
    update?
  end

  def index?
    true
  end

  class Scope
    attr_reader :administradora, :scope

    def initialize(administradora, scope)
      @administradora = administradora
      @scope = scope
    end

    # Las administradoras de grupo solo pueden administrar a sus
    # compañeras de grupo
    def resolve
      if administradora.super?
        scope.all
      else
        administradora.administradoras
      end
    end
  end
end
