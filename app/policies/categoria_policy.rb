class CategoriaPolicy < ApplicationPolicy
  attr_reader :administradora, :categoria

  def initialize(administradora, categoria)
    @administradora = administradora
    @categoria = categoria
  end

  # Todas las administradoras pueden ver las categorías, pero tiene un
  # scope por defecto
  def index?
    true
  end

  def new?
    create?
  end

  # Solo las administradoras crean categorías
  def create?
    administradora.super?
  end

  def edit?
    update?
  end

  def orden?
    administradora.super?
  end

  # Solo las administradoras pueden reordenar categorias
  def reordenar?
    administradora.super?
  end

  def ordenar_plantillas?
    administradora.categorias.include? categoria
  end

  # Las administradoras de grupo solo pueden modificar sus propias
  # categorías
  def update?
    administradora.super? || administradora.categorias.include?(categoria)
  end

  # Solo las administradoras pueden eliminar categorías
  def destroy?
    administradora.super?
  end

  class Scope
    attr_reader :administradora, :categorias

    def initialize(administradora, categorias)
      @administradora = administradora
      @categorias = categorias
    end

    # Las administradoras de grupo solo pueden ver sus categorias
    def resolve
      if administradora.super?
        categorias.all
      else
        administradora.categorias
      end
    end
  end
end
