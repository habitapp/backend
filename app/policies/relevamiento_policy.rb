class RelevamientoPolicy < ApplicationPolicy
  attr_reader :administradora, :relevamiento

  def initialize(administradora, relevamiento)
    @administradora = administradora
    @relevamiento = relevamiento
  end

  # Todas las administradoras pueden ver los relevamientos, pero tiene un
  # scope por defecto
  def index?
    true
  end

  def edit?
    update?
  end

  # Las administradoras de grupo solo pueden modificar sus propios
  # relevamientos
  def update?
    administradora.super? || administradora.categoria_ids.include?(relevamiento.categoria.id)
  end

  def destroy?
    update?
  end

  def confirm?
    update?
  end

  def show?
    update?
  end

  class Scope
    attr_reader :administradora, :relevamientos

    def initialize(administradora, relevamientos)
      @administradora = administradora
      @relevamientos = relevamientos
    end

    # Las administradoras de grupo solo pueden ver sus relevamientos
    def resolve
      if administradora.super?
        relevamientos.all
      else
        Relevamiento.where(categoria_id: administradora.categoria_ids)
      end
    end
  end
end
