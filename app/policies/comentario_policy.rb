class ComentarioPolicy < ApplicationPolicy
  attr_reader :administradora, :comentario

  def initialize(administradora, comentario)
    @administradora = administradora
    @comentario = comentario
  end

  # Las administradoras de grupo solo pueden modificar sus propios
  # comentarios
  def destroy?
    update?
  end

  def create?
    true
  end

  def edit?
    update?
  end

  def update?
    administradora.super? || administradora.categoria_ids.include?(comentario.relevamiento.categoria.id)
  end
end
