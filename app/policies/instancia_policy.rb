class InstanciaPolicy < ApplicationPolicy
  attr_reader :administradora, :instancia

  def initialize(administradora, instancia)
    @administradora = administradora
    @instancia = instancia
  end

  # Todas las administradoras pueden ver las instancias
  def index?
    true
  end

  def destroy?
    administradora.super? || Instancia.includes(:relevamientos)
      .where(relevamientos: { categoria_id: administradora.categoria_ids })
      .include?(instancia)
  end

  # Todas las administradoras pueden crear instancias
  def new?
    create?
  end

  def create?
    true
  end

  def edit?
    update?
  end

  def update?
    true
  end

  class Scope
    attr_reader :administradora, :instancias

    def initialize(administradora, instancias)
      @administradora = administradora
      @instancias = instancias
    end

    # Las administradoras de grupo solo pueden ver sus instancias
    def resolve
      if administradora.super?
        instancias.all
      else
        Instancia.includes(:relevamientos).where(relevamientos: { categoria_id: administradora.categoria_ids })
      end
    end
  end
end
