class SubscripcionPolicy < ApplicationPolicy
  attr_reader :administradora, :subscripcion

  def initialize(administradora, subscripcion)
    @administradora = administradora
    @subscripcion = subscripcion
  end

  # Todas las administradoras pueden ver los subscripcions, pero tiene un
  # scope por defecto
  def index?
    administradora.super?
  end
end
