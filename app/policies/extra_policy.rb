class ExtraPolicy < ApplicationPolicy

  attr_reader :administradora, :extra

  def initialize(administradora, extra)
    @administradora = administradora
    @extra = extra
  end

  # Todas las administradoras pueden ver los extras, pero tiene un
  # scope por defecto
  def index?
    true
  end

  def historico?
    true
  end

  # Las administradoras de grupo solo pueden ver sus propios extras
  def show?
    administradora.super? || administradora.categoria_ids.include?(extra.relevamiento.categoria_id)
  end

  def deshacer?
    show?
  end

  class Scope
    attr_reader :administradora, :extras

    def initialize(administradora, extras)
      @administradora = administradora
      @extras = extras
    end

    # Las administradoras de grupo solo pueden ver sus extras
    def resolve
      if administradora.super?
        extras.all
      else
        extras.includes(:relevamiento).where(relevamientos: { categoria_id: administradora.categoria_ids })
      end
    end
  end
end
