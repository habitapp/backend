class ExtraSerializer
  include FastJsonapi::ObjectSerializer

  attributes :required, :plantilla_id
  attribute :valor do |o|
    if o.valor == 't'
      'true'
    else
      o.valor
    end
  end

  attribute :label do |o|
    o.plantilla.try :label
  end

  attribute :name do |o|
    "control_#{o.plantilla_id}"
  end
end
