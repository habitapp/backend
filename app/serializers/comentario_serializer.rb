class ComentarioSerializer
  include FastJsonapi::ObjectSerializer

  attributes :contenido, :relevamiento_id
  attribute :confirmado do |o|
    o.comentarista.class == Administradora
  end
  attribute :created_at do |o|
    o.created_at.strftime('%s')
  end
end

