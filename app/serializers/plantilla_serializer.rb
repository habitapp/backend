class PlantillaSerializer
  include FastJsonapi::ObjectSerializer

  attributes :label, :value, :help, :photo, :required, :order, :name,
    :possible_values, :multiple
end
