class CategoriaSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :nombre, :arbol, :raiz_id, :relevamientos_count, :color, :total, :descripcion

  has_many :plantillas
end
