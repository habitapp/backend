class InstanciaSerializer
  include FastJsonapi::ObjectSerializer

  attribute :identifier
end
