class RelevamientoSerializer
  include FastJsonapi::ObjectSerializer

  attributes :descripcion, :confirmado, :direccion

  attribute :categoria_id do |o|
    if o.categoria.raiz_id
      o.categoria.raiz_id
    else
      o.categoria_id
    end
  end

  attribute :sub_categoria_id do |o|
    o.categoria_id
  end

  has_many :extras

  attribute :fotos do |o|
    fotos = {}
    fotos['principal'] = if foto = o.fotos.try(:first).try(:url)
      'https://habitapp.org' + foto
    else
      'https://habitapp.org/default.png'
    end

    o.extras.each do |e|
      next unless e.fotos.try(:first).try(:url)
      fotos["control_#{e.plantilla_id}"] = 'https://habitapp.org' + e.fotos.try(:first).try(:url)
    end

    fotos
  end

  attribute :thumbs do |o|
    fotos = {}
    fotos['principal'] = if foto = o.fotos.try(:first).try(:thumb).try(:url)
      'https://habitapp.org' + foto
    else
      'https://habitapp.org/default.png'
    end

    o.extras.each do |e|
      next unless e.fotos.try(:first).try(:thumb).try(:url)
      fotos["control_#{e.plantilla_id}"] = 'https://habitapp.org' + e.fotos.try(:first).try(:thumb).try(:url)
    end

    fotos
  end

  attribute :coordenadas do |o|
    { lat: o.coordenadas.try(:lat), lng: o.coordenadas.try(:lon) }
  end

  attribute :fecha do |o|
    o.created_at.strftime('%d/%m/%Y')
  end

  attribute :ultima_modificacion do |o|
    o.updated_at.strftime('%d/%m/%Y')
  end

  attributes :categoria do |o|
    { arbol: o.categoria.arbol, color: o.categoria.color }
  end
end
