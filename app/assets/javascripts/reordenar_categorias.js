$(document).on('turbolinks:load', function() {
  var table = document.querySelector('.table-draggable');

  if (table == null) return;

  tableDragger(table, {
    mode: 'row',
    onlyBody: true,
    dragHandler: '.handle'
  }).on('drop', function(from, to, el, mode) {
    // Al soltar, reordenamos todas las categorías
    $('.field.order').val(function(i,v) { return i; });
  });
});
