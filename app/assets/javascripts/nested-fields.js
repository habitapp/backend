$(document).on('turbolinks:load', function() {
  var removeGroup = function(e) {
    e.preventDefault();
    var group = $(this).parents('.field-group');

    // Si el grupo es nuevo solo lo eliminamos, sino lo marcamos para
    // elimininación
    if (group.hasClass('new')) {
      group.remove();
    } else {
      group.addClass('d-none');
      group.children('[name*="[_destroy]"]').val('true');
    }
  }

  var addGroup = function(e) {
  // Permite agregar más filas a los campos nested? && array?
    e.preventDefault();

    var _this = $(this);
    // Obtener el nombre del grupo para poder delimitar el campo de
    // acción
    var _own_group = _this.data('group');
    // Sube hasta la raíz del grupo
    var _groups = _this.parents('.field-groups.'+_own_group);
    // El grupo a clonar es el primero
    var _group_orig = _groups
      .children('.field-group')
      .first();

    // Deshabilita select2
    _group_orig.find('select.select-2').select2('destroy');
    _group_orig.find('select.select-2').data('select2-id', '');

    // Luego clona el grupo y lo agrega al final
    var _group = _group_orig
      .clone()
      .addClass('new')
      .removeClass('d-none')
      .appendTo(_groups);

    // Encuentra todos los elementos con '[0]' en el nombre y les pone
    // un ID generado a partir del tiempo Unix.  De esta forma se crea
    // un grupo que va a terminar en el mismo Hash de valores.
    var _date = (new Date).getTime();

    // Cambiar el ID
    _group.attr('id', _own_group+'-'+_date);

    // Filtramos por el grupo para ser mas especificas
    var _param = _own_group+'_attributes';
    _group.find('[name*="['+_param+']"]').each(function(i, input) {
      var _input = $(input);
      var _name  = _input.attr('name');
      var _regex = new RegExp('\\['+_param+'\\]\\[[0-9]+\\]');
      _input.attr('name', _name.replace(_regex, '['+_param+']['+_date+']'));
      if (['checkbox','radio'].includes(_input.attr('type'))) {
        _input.prop('checked', false);
      } else {
        _input.val('');
      }
    });

    // Actualizar las etiquetas también!
    _group.find('[for*="'+_param+'"]').each(function(i, label) {
      var _label = $(label);
      var _for   = _label.attr('for');
      var _regex = new RegExp('_'+_param+'_[0-9]+_');
      _label.attr('for', _for.replace(_regex, '_'+_param+'_'+_date+'_'));
    });

    // Y los ids!
    _group.find('[id*="'+_param+'"]').each(function(i, input) {
      var _input = $(input);
      var _id   = _input.attr('id');
      var _regex = new RegExp('_'+_param+'_[0-9]+_');
      _input.attr('id', _id.replace(_regex, '_'+_param+'_'+_date+'_'));
    });

    // Regenera el evento del botón de borrado
    _group.find('.remove-group').click(removeGroup);
    // Agregar el evento a los sub-botones
    _group.find('.add-group').click(addGroup);

    // Regenera los select2
    var _select2_opts = {
      language: 'es',
      theme: 'bootstrap',
      width: '',
      allowClear: true,
      containerCssClass: ':all:'
    };
    _group.find('.select-2').select2(_select2_opts);
    _group_orig.find('.select-2').select2(_select2_opts);
  };

  $('.add-group').click(addGroup);

  // Este evento permite remover las filas que ya existen
  $('.remove-group').click(removeGroup);
});
