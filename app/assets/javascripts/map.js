$(document).on('turbolinks:load', function() {
  if ($('#map').length == 0) return;
  var marker = null;

  L.ColorIcon = L.DivIcon.extend({
    options: {
      html: '<span class="fa fa-map-marker"></span>',
      className: 'marker'
    },

    createIcon: function(oldIcon) {
      var icon = L.DivIcon.prototype.createIcon.call(this, oldIcon);
      icon.style.color = this.options.color;

      return icon;
    }
  });

  L.colorIcon = function(options) {
    return new L.ColorIcon(options);
  }

  var map = L.map('map');
  // Argentina
  // map.setView([-40.55,-74.62],4);
  // CABA
  map.setView([-34.6107,-58.4246],4);

  L.tileLayer('https://osm.habitapp.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
  }).addTo(map);

  L.tileLayer('https://habitapp.org/parcelario/{z}/{x}/{y}.png', {
    attribution: '',
    minZoom: 15,
    maxZoom: 18,
    id: 'mapbox.streets',
  }).addTo(map);

  var lat = $('#map').data('lat'),
      lng = $('#map').data('lng'),
      color = $('#map').data('color');

  if (lat != undefined && lng != undefined) {
    marker = L.marker([lat,lng], {
      icon: L.colorIcon({color: color})
    }).addTo(map);

    marker.getElement().style.color = color;

    map.setView([lat,lng], 16);

    var lat_field = $('#relevamiento_coordenadas_lat'),
        lng_field = $('#relevamiento_coordenadas_lng');
    if (lat_field.length > 0) {
      map.on('click', function(e) {
        if (map.getZoom() < 15) return;
        map.removeLayer(marker);
        marker = L.marker(e.latlng, { icon: L.colorIcon({color: color })}).addTo(map);

        lat_field.val(e.latlng['lat']);
        lng_field.val(e.latlng['lng']);
      });
    }
  } else {
    var relevamientos = L.geoJSON(undefined, {
      pointToLayer: function(point, latlng) {
        return L.marker(latlng, {
          icon: L.colorIcon({ color: point.properties.color })
        });
      }
    }).addTo(map);
    // TODO al mover, enviar un filtro de la zona y agregar los puntos
    // dinámicamente
    $.ajax({
      dataType: 'json',
      url: '/api/relevamientos.geojson',
      success: function(data) {
        relevamientos.addData(data);
      }
    });
  }
});
