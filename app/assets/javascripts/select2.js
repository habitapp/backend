$(document).on('turbolinks:load', function() {
  if ($('.select2').size() > 0) return;
  $('.select-2').select2({
    language: 'es',
    theme: 'bootstrap',
    width: '',
    allowClear: true,
    containerCssClass: ':all:'
  });
});
