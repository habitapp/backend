window.toggle_possible_values = function(select) {
  var value = $(select);
  var possible_values = value.parents('.field-group').find('.possible-values');

  if (value.val() == 'select') {
    possible_values.removeClass('d-none');
  } else {
    possible_values.addClass('d-none');
  }
}
