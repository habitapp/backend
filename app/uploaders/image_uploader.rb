# Habilita la carga de imágenes
class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::BombShelter

  process :strip
  process resize_to_limit: [1280, 1280]

  version :thumb do
    process :strip
    process resize_to_fill: [200,200]
  end

  def extension_whitelist
    %w[jpg jpeg png]
  end

  # Si el nombre ya es un uuid, no cambiarlo
  def filename
    current_filename = parent_version.try(:filename) || file.try(:filename)
    @unique_filename ||= if /\A(thumb_)?[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\..*\Z/ =~ current_filename
      current_filename
    else
      [SecureRandom.uuid, '.', file.extension].join
    end
  end

  def strip
    manipulate! do |img|
      img.strip
      img
    end
  end
end
