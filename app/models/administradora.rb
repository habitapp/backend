class Administradora < ApplicationRecord
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, :lockable

  has_many :comentarios, as: :comentarista
  has_and_belongs_to_many :grupos
  has_many :administradoras, through: :grupos
  has_many :categorias, through: :grupos

  def to_s
    email.split('@').first
  end

  def super!
    self.update_attribute :super, true
  end

  def unsuper!
    self.update_attribute :super, false
  end
end
