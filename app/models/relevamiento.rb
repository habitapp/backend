class Relevamiento < ApplicationRecord
  has_logidze
  has_many   :comentarios
  belongs_to :categoria, counter_cache: true
  belongs_to :instancia, counter_cache: true
  has_many   :extras, dependent: :destroy, autosave: true

  accepts_nested_attributes_for :extras

  validates_presence_of :instancia, :categoria

  mount_uploaders :fotos, ImageUploader

  def foto_url
    "https://habitapp.org" + (fotos.try(:first).try(:url) || '/default.png')
  end

  def comentarios_to_plain_text
    comentarios.map do |c|
      "* #{c.contenido} -- #{c.comentarista} #{c.created_at} (#{c.privado})"
    end.join "\n"
  end

  def self.hash_to_wkt(coordenadas)
    return nil unless coordenadas['lng'].present? && coordenadas['lat'].present?
    "POINT(#{coordenadas['lng']} #{coordenadas['lat']})"
  end
end
