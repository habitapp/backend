class Categoria < ApplicationRecord
  has_many :relevamientos, dependent: :restrict_with_error
  has_many :plantillas, dependent: :restrict_with_error
  belongs_to :raiz, class_name: 'Categoria', optional: true
  has_many :sub_categorias, class_name: 'Categoria', foreign_key: 'raiz_id'
  has_and_belongs_to_many :grupos

  validates_presence_of :nombre

  accepts_nested_attributes_for :plantillas, allow_destroy: true

  before_save :update_arbol!
  before_save :ordenar_plantillas!
  after_save :update_arbol_in_sub_categorias!

  default_scope -> { order(:orden) }

  SUB_BASE = 1000000.freeze

  def to_s
    arbol
  end

  def update_arbol!
    self.arbol = raiz.present? ? "#{raiz} / #{nombre}" : nombre
  end

  def update_arbol_in_sub_categorias!
    sub_categorias.map(&:update_arbol!)
    sub_categorias.map(&:save)
  end

  def total
    relevamientos_count + (sub_categorias.pluck(:relevamientos_count).inject(:+) || 0)
  end

  def arbol_con_total
    "#{arbol} (#{total})"
  end

  def ordenar_plantillas!
    plantillas.each_with_index do |plantilla, index|
      plantilla.order = index
    end

    true
  end
end
