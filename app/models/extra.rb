class Extra < ApplicationRecord
  has_logidze
  belongs_to :relevamiento, touch: true
  belongs_to :instancia
  belongs_to :plantilla
  mount_uploaders :fotos, ImageUploader

  default_scope -> { includes(:plantilla).order('plantillas.order asc') }
end
