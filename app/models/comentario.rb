class Comentario < ApplicationRecord
  belongs_to :comentarista, polymorphic: true

  scope :publicos, -> { where(privado: false) }

  def to_s
    contenido
  end
end
