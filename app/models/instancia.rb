# Representa una instancia de la app, útil para hacer seguimiento de
# quién releva qué
class Instancia < ActiveRecord::Base
  validates_presence_of :password_digest, :identifier
  validates :identifier, uniqueness: true
  has_secure_password

  # Eliminar los relevamientos y comentarios junto con la instancia
  has_many :relevamientos, dependent: :destroy
  has_many :comentarios, dependent: :destroy, as: :comentarista

  before_validation :generate_identifier!, on: [:create]

  def short_identifier
    identifier.split('-').last
  end
  alias :to_s :short_identifier

  private

  def generate_identifier!
    self.identifier = SecureRandom.uuid unless self.identifier
  end
end
