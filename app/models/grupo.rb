class Grupo < ApplicationRecord
  has_and_belongs_to_many :administradoras
  has_and_belongs_to_many :categorias

  validates_presence_of :nombre

  def to_s
    nombre
  end
end
