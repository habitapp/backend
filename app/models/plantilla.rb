class Plantilla < ApplicationRecord
  belongs_to :categoria
  has_many :extras

  VALUES = %w[string number boolean select].freeze

  default_scope -> { order(:order) }

  before_save :remove_empty_values!

  def name
    "control_#{id}"
  end

  private

  def remove_empty_values!
    return true unless possible_values

    self.possible_values.reject! do |v|
      v.empty?
    end

    true
  end
end
