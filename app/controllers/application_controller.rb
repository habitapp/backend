class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :remember_filters, only: [:index, :orden]

  def index
  end

  # Necesario para que Pundit no use `current_user`
  def pundit_user
    current_administradora
  end

  def remember_filters
    session[request.path] = session[:filters] = request.original_fullpath
  end

  def redirect_to_filters_or(path)
    redirect_to session[:filters] || path
  end
end
