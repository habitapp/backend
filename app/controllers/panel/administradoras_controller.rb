class Panel::AdministradorasController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html

  def index
    authorize Administradora
    @administradoras = policy_scope(Administradora)
  end

  def new
    authorize Administradora
    @administradora = Administradora.new
  end

  # Cuando creamos una administradora, no elegimos la contraseña sino
  # que le enviamos el correo de recuperación para que se elija una
  def create
    authorize Administradora
    @administradora = Administradora.new administradora_params
    @administradora.password = SecureRandom.hex

    if @administradora.save
      @administradora.send_reset_password_instructions
    end

    respond_with :panel, @administradora
  end

  # no necesitamos ver una pagina con el mail solamente
  def show
    redirect_to panel_administradoras_path
  end

  def destroy
    find_administradora
    authorize @administradora

    @administradora.destroy
    if @administradora == current_administradora
      sign_out
    else
      respond_with :panel, @administradora
    end
  end

  def edit
    find_administradora
    authorize @administradora
  end

  # Si cambiamos el mail le enviamos una notificacion
  def update
    find_administradora
    authorize @administradora

    @administradora.update_attributes administradora_params

    if @administradora.email_changed?
      @administradora.send_email_changed_notification
    end

    respond_with :panel, @administradora
  end

  def lock
    find_administradora
    authorize @administradora
    @administradora.lock_access! send_instructions: false

    respond_with :panel, @administradora
  end

  def unlock
    find_administradora
    authorize @administradora
    @administradora.unlock_access!

    respond_with :panel, @administradora
  end

  def super
    find_administradora
    authorize @administradora
    @administradora.super!

    respond_with :panel, @administradora
  end

  def unsuper
    find_administradora
    authorize @administradora
    @administradora.unsuper!

    respond_with :panel, @administradora
  end

  private

  def find_administradora
    @administradora = Administradora.find(params[:id] || params[:administradora_id])
  end

  def administradora_params
    params.require(:administradora).permit(:email, grupo_ids: [])
  end
end
