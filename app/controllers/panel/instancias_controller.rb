class Panel::InstanciasController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html

  def index
    authorize Instancia
    @q = policy_scope(Instancia).ransack(params[:q])
    @instancias = @q.result.page(params[:page])
  end

  def destroy
    @instancia = Instancia.find(params[:id])
    authorize @instancia
    @instancia.destroy

    redirect_to_filters_or panel_instancias_path
  end

  def new
    authorize Instancia

    @instancia = Instancia.new
  end

  def create
    authorize Instancia
    @instancia = Instancia.new(instancia_params)

    if @instancia.save
      redirect_to_filters_or panel_instancias_path
    else
      render 'panel/instancias/new'
    end
  end

  def edit
    @instancia = Instancia.find(params[:instancia_id] || params[:id])
    authorize @instancia
  end

  def update
    @instancia = Instancia.find(params[:instancia_id] || params[:id])
    authorize @instancia

    if @instancia.save
      redirect_to_filters_or panel_instancias_path
    else
      render 'panel/instancias/edit'
    end
  end

  private

  def instancia_params
    params.require(:instancia).permit(:identifier, :password)
  end
end
