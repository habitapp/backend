class Panel::CategoriasController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html

  def index
    @categorias = policy_scope(Categoria)
  end

  def orden
    authorize Categoria
    if params[:categoria].blank?
      @categorias = Categoria.where(raiz_id: nil)
    else
      @categoria = Categoria.find(params[:categoria])
      @categorias = Categoria.where(raiz_id: params[:categoria])
    end
  end

  # Reordenar las categorías y subcategorías
  #
  # Al reordenar las categorías, necesitamos ubicar sub categorías justo
  # debajo de la categoría, recursivamente.
  def reordenar
    authorize Categoria
    p = params[:categorias][:orden]

    # Encontrar todas las categorías a ordenar, pueden ser raíz o sub
    # categorías o sub sub categorias
    @categorias = Categoria.where(id: p.keys)
    # Ejecutar dentro de una sola transacción
    Categoria.transaction do
      # Recorrerlas todas
      @categorias.each do |c|
        # Actualizar el orden a partir de la raíz y convertimos a un
        # decimal dentro de ese orden
        orden = if c.raiz
          c.raiz.orden + (p[c.id.to_s].to_f + 1.0) / Categoria::SUB_BASE
        # O si no hay raíz, usar un número entero.  Sumamos uno para no
        # empezar de 0
        else
          p[c.id.to_s].to_f + 1.0
        end

        # Actualizamos el orden
        c.update_attribute :orden, orden
        # Y también actualizamos el orden de las sub categorías
        c.sub_categorias.order(:orden).each_with_index do |s,i|
          s.update_attribute :orden, orden + (i.to_f + 1.0) / Categoria::SUB_BASE
        end
      end
    end

    redirect_to_filters_or panel_categorias_path
  end

  def new
    authorize Categoria
    @categoria = Categoria.new
    @categoria.raiz_id = params[:raiz] if params[:raiz].present?

    @categoria.color = "#%06x" % (rand * 0xffffff)
    @categoria.plantillas.build
  end

  def create
    authorize Categoria
    @categoria = Categoria.create categoria_params

    respond_with :panel, @categoria
  end

  # Por ahora no queremos ver ninguna categoría individual
  def show
    redirect_to_filters_or panel_categorias_path
  end

  def destroy
    find_categoria
    authorize @categoria
    @categoria.destroy
    respond_with :panel, @categoria
  end

  def edit
    find_categoria
    authorize @categoria

    @categoria.plantillas.build if @categoria.plantillas.empty?
  end

  def update
    find_categoria
    authorize @categoria
    @categoria.update_attributes categoria_params
    respond_with :panel, @categoria
  end

  private

  def categoria_params
    params
      .require(:categoria)
      .permit(:raiz_id, :nombre, :color, :descripcion, :confidencial,
        plantillas_attributes: [:id, :_destroy, :label, :help,
          :required, :photo, :value, :multiple, possible_values: [] ])
  end

  def find_categoria
    @categoria = Categoria.find(params[:id] || params[:categoria_id])
  end
end
