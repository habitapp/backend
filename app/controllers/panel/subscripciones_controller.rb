require 'csv'

class Panel::SubscripcionesController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html, :csv

  def index
    authorize Subscripcion
    @subscripciones = Subscripcion.all

    respond_to do |f|
      f.html do
        @subscripciones = @subscripciones.page(params[:page])
      end

      f.csv do
        headers['Content-Disposition'] = "attachment; filename=\"subscripciones.csv\""
        headers['Content-Type'] ||= 'text/csv; charset=utf-8'
        @csv_options = { col_sep: ';', quote_char: '"', force_quotes: true }
        @headers = %w[id email created_at]

        render layout: false
      end
    end
  end
end
