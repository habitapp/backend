class Panel::PlantillasController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html

  def orden
    @categoria = Categoria.find(params[:categoria_id])
    authorize @categoria, :ordenar_plantillas?
    @plantillas = @categoria.plantillas
  end

  def reordenar
    @categoria = Categoria.find(params[:categoria_id])
    authorize @categoria, :ordenar_plantillas?
    @plantillas = @categoria.plantillas

    order = params.require(:plantillas).permit(order: {})
    Plantilla.transaction do
      @plantillas.each do |p|
        p.update_attribute :order, order[:order][p.id.to_s]
      end
    end

    redirect_to_filters_or panel_categoria_plantillas_orden_path(@categoria)
  end
end
