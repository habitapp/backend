require 'csv'

class Panel::RelevamientosController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html, :csv, :geojson

  def index
    authorize Relevamiento
    @q = policy_scope(Relevamiento).ransack(params[:q])
    @relevamientos = @q.result
    # Los extras solo se muestran si estamos filtrando por categoría
    @extras = params[:extras].present? && params[:q][:categoria_id_eq].present?
    # Las subcategorías se incluyen opcionalmente
    @sub_categorias = params[:sub_categorias].present? && params[:q][:categoria_id_eq].present?
    @categoria = Categoria.find(params[:q][:categoria_id_eq]) if @extras || @sub_categorias

    @relevamientos = @relevamientos.includes(:extras) if @extras

    if @sub_categorias
      @relevamientos = @relevamientos.or(Relevamiento.where(categoria_id: @categoria.sub_categoria_ids))
    end

    @relevamientos = @relevamientos.includes(:instancia, :comentarios, categoria: [ :plantillas ])

    respond_to do |f|
      f.html do
        @relevamientos = @relevamientos.page(params[:page])
      end

      f.csv do
        headers['Content-Disposition'] = "attachment; filename=\"relevamientos.csv\""
        headers['Content-Type'] ||= 'text/csv; charset=utf-8'
        @csv_options = { col_sep: ';', quote_char: '"', force_quotes: true }
        @headers = %w[id coordenadas.lat coordenadas.lon categoria.arbol
          categoria.color confirmado direccion descripcion
          informacion_de_contacto comentarios_to_plain_text foto_url
          created_at updated_at]

        render layout: false
      end

      # XXX Qué tan pesado será esto?
      f.geojson do
        features = []
        @relevamientos.each do |r|
          extras = {}
          if @extras
            extras = Hash[r.extras.map { |e| [e.plantilla.label, e.valor] }]
          end

          features << RGeo::GeoJSON::Feature.new(r.coordenadas, r.id,
            r.attributes
             .merge({ color: r.categoria.color,
                      categoria: r.categoria.arbol,
                      comentarios: r.comentarios_to_plain_text.split("\n")})
             .merge(extras))
        end
        collection = RGeo::GeoJSON::FeatureCollection.new(features)

        render json: RGeo::GeoJSON.encode(collection)

      end
    end
  end

  def show
    @relevamiento = Relevamiento.find(params[:id])
    authorize @relevamiento
  end

  def confirm
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    authorize @relevamiento
    @relevamiento.update_attribute :confirmado, true

    redirect_to_filters_or panel_relevamiento_path(@relevamiento)
  end

  def destroy
    @relevamiento = Relevamiento.find(params[:id])
    authorize @relevamiento
    @relevamiento.destroy

    redirect_to_filters_or panel_relevamiento_path(@relevamiento)
  end

  def edit
    @relevamiento = Relevamiento.find(params[:id])
    authorize @relevamiento
  end

  def update
    @relevamiento = Relevamiento.find(params[:id])
    authorize @relevamiento
    p = relevamiento_params
    coordenadas = Relevamiento.hash_to_wkt(p.delete(:coordenadas))
    p[:coordenadas] = coordenadas if coordenadas

    @relevamiento.update_attributes(p)

    redirect_to_filters_or panel_relevamiento_path(@relevamiento)
  end

  private

  def relevamiento_params
    params.require(:relevamiento).permit(:confirmado, :descripcion,
      :direccion, :informacion_de_contacto, :categoria_id,
      :remove_fotos,
      fotos: [],
      extras_attributes: [ :valor, :id ],
      coordenadas: {})
  end
end
