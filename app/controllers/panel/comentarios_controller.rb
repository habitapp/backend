class Panel::ComentariosController < ApplicationController
  before_action :authenticate_administradora!

  def create
    authorize Comentario
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentario = @relevamiento.comentarios.create(comentario_params)
    @comentario.comentarista = current_administradora

    @comentario.save

    redirect_to [:panel,@relevamiento]
  end

  def edit
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentario = @relevamiento.comentarios.find(params[:id])
    authorize @comentario
  end

  def update
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentario = @relevamiento.comentarios.find(params[:id])
    authorize @comentario

    @comentario.update_attributes comentario_params

    redirect_to [:panel, @relevamiento]
  end

  def destroy
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentario = @relevamiento.comentarios.find(params[:id])
    authorize @comentario

    @comentario.destroy

    redirect_to [:panel, @relevamiento]
  end

  private
  def comentario_params
    params.require(:comentario).permit(:contenido, :privado)
  end
end
