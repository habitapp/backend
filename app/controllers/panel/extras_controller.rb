require 'csv'

class Panel::ExtrasController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html, :csv, :geojson

  def index
    authorize Extra
    @q = policy_scope(Extra).includes(:plantilla, :instancia, relevamiento: [ :categoria ]).ransack(params[:q])
    @extras = @q.result
    @historico = params[:historico].present?

    respond_to do |f|
      f.html do
        @extras = @extras.page(params[:page])
      end

      f.csv do
        headers['Content-Disposition'] = "attachment; filename=\"extras.csv\""
        headers['Content-Type'] ||= 'text/csv; charset=utf-8'
        @csv_options = { col_sep: ';', quote_char: '"', force_quotes: true }
        @headers = %w[id log_version relevamiento.categoria.arbol
          relevamiento.instancia.short_identifier relevamiento.id
          plantilla.label valor created_at updated_at]

        render layout: false
      end
    end
  end

  def historico
    authorize Extra
    @q = policy_scope(Extra).unscoped.includes(:plantilla, :instancia, relevamiento: [ :categoria ]).ransack(params[:q])
    # Agrupar los extras por relevamiento.  Ya vienen agrupados por
    # plantilla así que no es necesario.
    @extras = @q.result.sort_by do |e|
      e.plantilla.order
    end.group_by do |e|
      e.relevamiento_id
    end
    # Necesitamos saber la fecha mayor y menor
    @from = @q.result.order(:created_at).try(:first).try(:created_at)
    @to   = @q.result.order(:updated_at).try(:last).try(:updated_at)
    begin
      @range = (@from.try(:to_date)..@to.try(:to_date)).to_a
    rescue ArgumentError, TypeError
      @range = []
    end

    respond_to do |f|
      f.html do
      end

      f.csv do
        headers['Content-Disposition'] = "attachment; filename=\"historico.csv\""
        headers['Content-Type'] ||= 'text/csv; charset=utf-8'
        @headers = %w[relevamiento_id plantilla.label]
        @csv_options = { col_sep: ';', quote_char: '"', force_quotes: true }

        render layout: false
      end

      f.geojson do
        features = []

        # TODO esta algoritmo se repite por cada vista...
        @extras.each do |grupo|
          grupo.try(:last).try(:each) do |extra|
            attributes = {
              t('.relevamiento_id') => grupo.try(:first),
              t('.plantilla.label') => extra.try(:plantilla).try(:label)
            }.merge(
              Hash[@range.map do |date|
                [date, if extra.try(:updated_at).try(:to_date) == date
                  extra.try(:valor)
                else
                  extra.at(time: date).try(:valor)
                end]
              end])
            features << RGeo::GeoJSON::Feature.new(extra.relevamiento.coordenadas, extra.id, attributes)
          end
        end

        collection = RGeo::GeoJSON::FeatureCollection.new(features)

        render json: RGeo::GeoJSON.encode(collection)
      end
    end
  end

  def show
    @extra = policy_scope(Extra).find(params[:id])
    authorize @extra
  end

  def deshacer
    @extra = policy_scope(Extra).find(params[:extra_id])
    authorize @extra

    if @extra.switch_to! params[:version].to_i
      flash[:success] = t('.undo_success')
    else
      flash[:warning] = t('.undo_error')
    end

    redirect_to panel_extra_path(@extra)
  end
end
