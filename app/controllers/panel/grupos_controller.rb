class Panel::GruposController < ApplicationController
  before_action :authenticate_administradora!
  respond_to :html

  def index
    @grupos = policy_scope(Grupo)
  end

  def new
    authorize Grupo
    @grupo = Grupo.new
  end

  def create
    authorize Grupo
    @grupo = Grupo.create(grupo_params)

    respond_with :panel, @grupo
  end

  def show
    authorize Grupo
    @grupo = Grupo.find(params[:id])
  end

  def edit
    authorize Grupo
    @grupo = Grupo.find(params[:id])
  end

  def update
    authorize Grupo
    @grupo = Grupo.find(params[:id])
    @grupo.update_attributes(grupo_params)

    respond_with :panel, @grupo
  end

  def destroy
    authorize Grupo
    @grupo = Grupo.find(params[:id])
    @grupo.destroy

    respond_with :panel, @grupo
  end

  private

  def grupo_params
    params.require(:grupo).permit(:nombre, administradora_ids: [], categoria_ids: [])
  end

end
