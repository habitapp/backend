# Trabajar con relevamientos
class Api::RelevamientosController < Api::BaseController
  before_action :authenticate_token, only: [:create,:update]
  respond_to :json, :geojson
  # TODO: nunca devolver todos los puntos, siempre filtrarlos por zona
  # de búsqueda
  def index
    scope = Relevamiento.includes(:categoria)
    scope = scope.where(categoria_id: params[:categoria].split(',').compact.uniq) if params[:categoria].present?
    # Nunca se muestran los relevamientos confidenciales!
    scope = scope.where(categoria_id: Categoria.where(confidencial: false))

    respond_to do |format|
      format.json do
        render json: serialize(scope)
      end

      format.geojson do
        features = scope.map do |c|
          RGeo::GeoJSON::Feature.new c.coordenadas, c.id, { color: c.categoria.color }
        end
        collection = RGeo::GeoJSON::FeatureCollection.new features

        render json: RGeo::GeoJSON.encode(collection)
      end
    end
  end

  def show
    @relevamiento = Relevamiento.find(params[:id])

    if @relevamiento
      render json: serialize(@relevamiento, include: [:extras])
    else
      render json: {}, status: :notfound
    end
  end

  def create
    p = relevamiento_params
    c = p.delete :coordenadas
    e = p.delete :extra
    f = p.delete :fotos

    @relevamiento = @instancia.relevamientos.new(p)
    @relevamiento.coordenadas = Relevamiento.hash_to_wkt(c)
    fotos = process_fotos(f)
    @relevamiento.fotos = fotos['principal']
    @relevamiento.extras = process_extra(e, @instancia, fotos)

    if @relevamiento.save
      render json: serialize(@relevamiento), status: :created
    else
      render json: {}, status: :unprocessable_entity
    end

    delete_fotos(fotos)
  end

  # Cualquier instancia puede actualizar un relevamiento, pero queda un
  # historial gracias a logidze
  def update
    @relevamiento = Relevamiento.includes(extras: [ :plantilla ]).find(params[:data][:id])
    p = relevamiento_params
    # No se modifican las coordenadas así que no las necesitamos
    p.delete :coordenadas
    f = p.delete :fotos
    e = p.delete :extra

    # Las fotos se suman y los extra se mergean
    new_fotos = process_fotos(f)
    @relevamiento.fotos = new_fotos['principal'] unless new_fotos['principal'].blank?
    # Actualizar los valores si ya estaban definidos y también la
    # instancia para tener una cadena de responsabilidades.
    @relevamiento.extras.each do |extra|
      key = extra.plantilla.name
      new_valor = e[key]

      unless new_fotos[key].blank?
        extra.fotos = new_fotos[key]
      end

      unless new_valor.blank?
        extra.valor = new_valor
        extra.instancia = @instancia
        # Eliminar el valor viejo para poder terminar con un listado de
        # valores nuevos
        e.delete key
      end
    end

    # Agregar los extra nuevos
    @relevamiento.extras = @relevamiento.extras + process_extra(e, @instancia, new_fotos)
    # Actualizar el resto de parámetros
    @relevamiento.assign_attributes(p)
    # Guardar
    if @relevamiento.save
      render json: serialize(@relevamiento), status: :ok
    else
      render json: {}, status: :unprocessable_entity
    end

    # Eliminar las fotos temporales
    delete_fotos(new_fotos)
  end

  private

  def process_extra(extra, instancia, fotos = {})
    [extra.try(:to_hash).try(:map) do |n, v|
      Extra.new(plantilla_id: n.split('_').last.to_i, valor: v, instancia: instancia, fotos: fotos.dig(n))
    end].flatten.compact
  end

  def process_fotos(fotos)
    fotos.to_hash.map do |nombre, foto|
      next if foto.empty?
      # No confiamos en el tipo de imagen enviado en los metadatos,
      # decodificamos y luego comprobamos el tipo de datos
      data = Base64.decode64(foto.split(';base64,').last.strip)
      f = Tempfile.new(%w[image .tmp])
      f.binmode
      f.write(data)
      f.rewind
      f.close

      type = `/usr/bin/file --mime-type "#{f.path}"`.split(' ').last
      ext = type.split('/').last

      # Si es un archivo de imagen, devolver el archivo leido
      upload = nil
      if %w[jpg jpeg png].include? ext
        file = f.path.gsub(/\.tmp\Z/, ".#{ext}")
        FileUtils.mv(f.path, file)
        upload = File.open(file)
      end

      f.unlink
      { nombre => [upload] } if upload
    end.compact.reduce({}, :merge)
  end

  def delete_fotos(fotos)
    fotos.map { |n,f| File.unlink(f.first.path) }
  end

  def relevamiento_params
    params.require(:data)
      .require(:attributes)
      .permit(:descripcion, :direccion, :informacion_de_contacto,
        :categoria_id,
        :raiz_id,
        fotos: {},
        coordenadas: {},
        extra: {})
  end

  def serialize(relevamiento, *options)
    RelevamientoSerializer.new(relevamiento, *options).serialized_json
  end
end
