class Api::InstanciasController < Api::BaseController
  # Registrar una instancia, esto lo tiene que tiene que hacer una vez
  # cada app y luego guardarse la contraseña y el identifier para
  # loguearse y obtener un token
  def create
    @instancia = Instancia.create(instancia_params)
    if @instancia.save
      render json: InstanciaSerializer.new(@instancia).serialized_json, status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # Loguearse con identifier + contraseña, respondemos con un JWT o con
  # error
  def login
    @instancia = Instancia.find_by(identifier: params[:instancia][:identifier])
    if @instancia && @instancia.authenticate(params[:instancia][:password])
      token = jwt_encode(identifier: @instancia.identifier)

      render json: { access_token: token }
    else
      render json: {}, status: :unauthorized
    end
  end

  private

  def instancia_params
    params.require(:instancia).permit(:password)
  end
end
