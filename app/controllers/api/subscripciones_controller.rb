class Api::SubscripcionesController < Api::BaseController
  before_action :authenticate_token, only: [:create]

  def create
    @subscripcion = Subscripcion.new(subscripcion_params)
    if @subscripcion.save
      render json: serialize(@subscripcion), status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private

  def subscripcion_params
    params.require(:data).require(:attributes).permit(:email)
  end

  def serialize(subscripcion)
    SubscripcionSerializer.new(subscripcion).serialized_json
  end
end
