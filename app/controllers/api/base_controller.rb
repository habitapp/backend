class Api::BaseController < ActionController::Base
  include ExceptionHandler

  # Autentica el token y devuelve una usuaria
  def authenticate_token
    payload = jwt_decode(bearer)
    @instancia = Instancia.find_by_identifier(payload.dig(:identifier))
  end

  private

  # Codificar el JWT
  def jwt_encode(payload)
    # Los tokens expiran en un día
    payload[:exp] = 1.day.from_now.to_i

    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  # Decodificar o devolver una excepción gestionada por
  # app/controller/concerns/exception_handler.rb
  #
  # XXX no será overkill?
  def jwt_decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base).first
    HashWithIndifferentAccess.new body
  rescue JWT::ExpiredSignature => e
    raise ExceptionHandler::ExpiredSignature, e.message
  rescue JWT::DecodeError, JWT::VerificationError => e
    raise ExceptionHandler::InvalidToken, e.message
  end

  # Obtener el token desde los headers
  def bearer
    request.headers['Authorization'].try(:split, ' ', 2).try(:last)
  end
end
