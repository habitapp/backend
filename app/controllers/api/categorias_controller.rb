class Api::CategoriasController < Api::BaseController
  def index
    # Rescatamos los fallos de autenticación porque queremos mostrar las
    # confidenciales solo a las instancias, no por un tema de seguridad
    # porque cualquiera puede registrar una instancia, sino para
    # diferenciar momentos en que se pueden ver y momentos en los que
    # no.
    begin
      authenticate_token
      render json: serialize(Categoria.all.includes(:sub_categorias))
    # XXX no estamos usando todas estas excepciones en
    # Api::BaseController pero las dejamos para el futuro.
    rescue ExceptionHandler::AuthenticationError,
      ExceptionHandler::MissingToken,
      ExceptionHandler::InvalidToken,
      ExceptionHandler::ExpiredSignature,
      ExceptionHandler::DecodeError
      render json: serialize(Categoria.where(confidencial: false))
    end
  end

  def show
    @categoria = Categoria.find(params[:id])

    if @categoria
      render json: serialize(@categoria, { include: [ :plantillas ] })
    else
      render json: {}, status: :notfound
    end
  end

  private

  def serialize(categoria, options = {})
    CategoriaSerializer.new(categoria, options).serialized_json
  end
end
