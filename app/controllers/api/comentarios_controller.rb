class Api::ComentariosController < Api::BaseController
  before_action :authenticate_token, only: [:create]

  def index
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentarios = @relevamiento.comentarios.publicos

    render json: serialize(@comentarios)
  end

  def create
    @relevamiento = Relevamiento.find(params[:relevamiento_id])
    @comentario = @relevamiento.comentarios.new(comentario_params)
    @comentario.comentarista = @instancia
    @comentario.privado = false

    if @comentario.save
      render json: serialize(@comentario), status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private

  def serialize(comentarios)
    ComentarioSerializer.new(comentarios).serialized_json
  end

  def comentario_params
    params.require(:data).require(:attributes).permit(:contenido)
  end
end
