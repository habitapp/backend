.DEFAULT_GOAL := all

d ?= web
c ?= $(shell date +%F | tr "-" ".")
r ?=
name = habitapp/$(d):$(c)
ifdef r
name := $(name).$(r)
endif

clean:
	rm -f log/*.log
	rm -rf tmp/cache/assets

build:
	docker build -t $(name) .

tag:
	docker tag $(name) registry.forja.lainventoria.com.ar/$(name)
	docker tag $(name) registry.forja.lainventoria.com.ar/habitapp/$(d):latest

push:
	docker push registry.forja.lainventoria.com.ar/$(name)
	docker push registry.forja.lainventoria.com.ar/habitapp/$(d):latest

all: clean build tag push
