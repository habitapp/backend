Rails.application.configure do
  config.i18n.available_locales = [:en, :es]
  config.i18n.default_locale = :es
end
