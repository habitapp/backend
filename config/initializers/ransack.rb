Ransack.configure do |c|
  c.custom_arrows = {
    up_arrow: '<i class="fa fa-sort-asc"></i>',
    down_arrow: '<i class="fa fa-sort-desc"></i>',
    default_arrow: '<i class="fa fa-unsorted"></i>'
  }
end
