CarrierWave.configure do |config|
  config.permissions = 0640
  config.directory_permissions = 0750
  config.storage = :file
end
