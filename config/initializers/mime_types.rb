Mime::Type.register 'application/vnd.geo+json', :geojson
Mime::Type.register 'application/json', :json,
  %w[ application/vnd.api+json text/x-json application/json ]
