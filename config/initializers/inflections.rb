[:en, :es].each do |locale|
  ActiveSupport::Inflector.inflections(locale) do |inflect|
    inflect.plural 'categoria', 'categorias'
    inflect.singular 'categorias', 'categoria'
    inflect.plural 'instancia', 'instancias'
    inflect.singular 'instancias', 'instancia'
    inflect.plural 'subscripcion', 'subscripciones'
    inflect.singular 'subscripciones', 'subscripcion'
  end
end
