Rails.application.routes.draw do
  devise_for :administradoras
  root 'application#index'

  namespace :api do
    resources :categorias, only: [:index, :show]
    resources :relevamientos, only: [:index,:create,:update,:show] do
      resources :comentarios, only: [:index, :create]
    end
    resources :instancias, only: [:create]
    post 'instancias/login', to: 'instancias#login'
    resources :subscripciones, only: [:create]
  end

  namespace :panel do
    get '/', to: 'administradoras#index'

    resources :subscripciones, only: [:index]
    get :'extras/historico', to: 'extras#historico'
    resources :extras, only: [:index, :show] do
      post :deshacer, to: 'extras#deshacer'
    end

    resources :administradoras do
      post :unlock, to: 'administradoras#unlock'
      post :lock, to: 'administradoras#lock'
      post :super, to: 'administradoras#super'
      post :unsuper, to: 'administradoras#unsuper'
    end

    resources :grupos
    get 'categorias/orden', to: 'categorias#orden'
    post 'categorias/reordenar', to: 'categorias#reordenar'
    resources :categorias do
      get 'plantillas/orden', to: 'plantillas#orden'
      post 'plantillas/reordenar', to: 'plantillas#reordenar'
    end
    resources :instancias

    resources :relevamientos do
      post :confirm, to: 'relevamientos#confirm'
      resources :comentarios
    end
  end
end
