class Typo < ActiveRecord::Migration[5.1]
  def change
    rename_column :comentarios, :relevamento_id, :relevamiento_id
  end
end
