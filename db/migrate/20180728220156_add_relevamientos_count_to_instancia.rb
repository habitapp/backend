class AddRelevamientosCountToInstancia < ActiveRecord::Migration[5.1]
  def change
    add_column :instancias, :relevamientos_count, :integer
    add_column :instancias, :comentarios_count, :integer
  end
end
