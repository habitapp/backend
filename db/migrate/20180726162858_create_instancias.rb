class CreateInstancias < ActiveRecord::Migration[5.1]
  def change
    create_table :instancias do |t|
      t.timestamps
      t.string :password_digest
      t.string :identifier, unique: true, index: true
    end

    add_column :relevamientos, :instancia_id, :integer, index: true
    add_column :comentarios, :instancia_id, :integer, index: true
  end
end
