class AddOrderToPlantillas < ActiveRecord::Migration[5.1]
  def up
    add_column :plantillas, :order, :integer

    Categoria.find_each do |c|
      c.plantillas.each_with_index do |p, i|
        p.update_attribute :order, i
      end
    end
  end

  def down
    remove_column :plantillas, :order
  end
end
