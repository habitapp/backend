class AddConfidencialToCategoria < ActiveRecord::Migration[5.1]
  def change
    add_column :categorias, :confidencial, :boolean, default: false, index: true
  end
end
