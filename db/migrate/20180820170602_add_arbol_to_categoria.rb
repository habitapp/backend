class AddArbolToCategoria < ActiveRecord::Migration[5.1]
  def up
    add_column :categorias, :arbol, :string

    Categoria.find_each do |c|
      c.save
    end
  end

  def down
    remove_column :categorias, :arbol, :string
  end
end
