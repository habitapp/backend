class AddCategoriaToRelevamiento < ActiveRecord::Migration[5.1]
  def change
    add_column :relevamientos, :categoria_id, :integer, index: true
  end
end
