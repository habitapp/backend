class Timestamps < ActiveRecord::Migration[5.1]
  def change
    [:relevamientos,:comentarios,:categorias].each do |table|
      change_table table do |t|
        t.timestamps null: false
      end
    end
  end
end
