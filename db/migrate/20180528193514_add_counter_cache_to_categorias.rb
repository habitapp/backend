class AddCounterCacheToCategorias < ActiveRecord::Migration[5.1]
  def change
    add_column :categorias, :relevamientos_count, :integer, default: 0
  end
end
