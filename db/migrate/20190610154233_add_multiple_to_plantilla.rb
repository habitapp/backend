class AddMultipleToPlantilla < ActiveRecord::Migration[5.1]
  def change
    add_column :plantillas, :multiple, :boolean, default: false
  end
end
