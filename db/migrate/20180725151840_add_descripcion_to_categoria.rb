class AddDescripcionToCategoria < ActiveRecord::Migration[5.1]
  def change
    add_column :categorias, :descripcion, :text
  end
end
