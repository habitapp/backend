class AddRequiredToExtras < ActiveRecord::Migration[5.1]
  def change
    add_column :extras, :required, :boolean
  end
end
