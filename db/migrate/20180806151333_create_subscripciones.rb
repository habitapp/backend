class CreateSubscripciones < ActiveRecord::Migration[5.1]
  def change
    create_table :subscripciones do |t|
      t.timestamps
      t.string :email
    end
  end
end
