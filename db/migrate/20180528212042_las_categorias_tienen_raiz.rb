class LasCategoriasTienenRaiz < ActiveRecord::Migration[5.1]
  def change
    rename_column :categorias, :madre_id, :raiz_id
  end
end
