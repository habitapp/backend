class AddFotosToExtras < ActiveRecord::Migration[5.1]
  def change
    add_column :extras, :fotos, :json
  end
end
