class AddPossibleValuesToPlantilla < ActiveRecord::Migration[5.1]
  def change
    add_column :plantillas, :possible_values, :jsonb
  end
end
