class MergeDuplicates < ActiveRecord::Migration[5.1]
  def up
    dups = Relevamiento.all.group_by do |r|
      # Encontrar duplicados por coordenadas y categorías
      [r.coordenadas, r.categoria_id]
    end.map(&:last).select do |g|
      g.size > 1
    end

    dups.each do |group|
      group.sort_by! &:created_at
      # Eliminar todos menos el último
      group.shift
      group.each do |relevamiento|
        relevamiento.destroy
      end
    end
  end

  def down
  end
end
