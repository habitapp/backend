class CreatePlantilla < ActiveRecord::Migration[5.1]
  def change
    create_table :plantillas do |t|
      t.belongs_to :categoria, index: true
      t.timestamps

      t.string :label
      t.string :value
      t.string :help
      t.boolean :photo
      t.boolean :required
    end
  end
end
