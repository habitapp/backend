class AddCoordenadasGeograficas < ActiveRecord::Migration[5.1]
  def change
    remove_column :relevamientos, :coordenadas, :geometry, srid: 3857
    add_column :relevamientos, :coordenadas, :geometry,
      srid: 4326, geographic: true
    add_index :relevamientos, :coordenadas, using: :gist
  end
end
