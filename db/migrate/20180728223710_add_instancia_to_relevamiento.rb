class AddInstanciaToRelevamiento < ActiveRecord::Migration[5.1]
  def up
    Instancia.transaction do
      instancia = Instancia.create(password: ENV['DEFAULT_ADMIN_PASSWORD'])

      Relevamiento.where(instancia_id: nil).find_each do |r|
        r.update_attribute :instancia_id, instancia.id
      end

      Comentario.where(instancia_id: nil).find_each do |c|
        c.update_attribute :instancia_id, instancia.id
      end
    end
  end

  def down
  end
end
