class CompactarExtras < ActiveRecord::Migration[5.1]
  def up
    Relevamiento.transaction do
      # Encontrar todos los relevamientos y compactar sus extras cuando
      # comparten la misma plantilla
      Relevamiento.includes(:extras).find_each do |relevamiento|
        # Agrupar los extras por su plantilla
        relevamiento.extras.group_by(&:plantilla_id).each do |_, extras|
          # Ordenarlos por fecha de creación
          extras.sort_by!(&:created_at)
          # Modificar el primero
          primero = extras.shift

          # Recorrer el resto
          extras.each do |extra|
            # No crear revisiones si no hacen modificaciones
            # sustanciales
            next if primero.valor == extra.valor && primero.fotos == extra.fotos

            # Guardar los cambios
            primero.updated_at   = extra.created_at
            primero.valor        = extra.valor
            primero.instancia_id = extra.instancia_id
            primero.fotos        = extra.fotos
            primero.save
          end

          # Eliminar los extras sobrantes
          extras.map &:destroy
        end
      end
    end
  end

  def down
  end
end
