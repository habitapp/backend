class AddSuperToAdministradoras < ActiveRecord::Migration[5.1]
  def up
    add_column :administradoras, :super, :boolean, index: true

    Administradora.find_each do |a|
      a.update_attribute :super, true
    end
  end

  def down
    remove_column :administradoras, :super
  end
end
