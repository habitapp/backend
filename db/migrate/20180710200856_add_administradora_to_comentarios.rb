class AddAdministradoraToComentarios < ActiveRecord::Migration[5.1]
  def change
    add_belongs_to :comentarios, :administradora, index: true
  end
end
