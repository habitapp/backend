class AddComentaristaToComentarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :comentarios, :administradora_id
    remove_column :comentarios, :instancia_id

    add_column :comentarios, :comentarista_type, :string
    add_column :comentarios, :comentarista_id, :bigint

    add_index :comentarios, [:comentarista_type,:comentarista_id]
  end
end
