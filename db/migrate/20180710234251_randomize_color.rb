class RandomizeColor < ActiveRecord::Migration[5.1]
  def up
    Categoria.find_each do |c|
      # https://stackoverflow.com/a/1698364
      c.update_attribute :color, "#%06x" % (rand * 0xffffff)
    end
  end
end
