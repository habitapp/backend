class AddConfirmacionToRelevamientos < ActiveRecord::Migration[5.1]
  def change
    add_column :relevamientos, :confirmado, :boolean, default: false
  end
end
