class CreateRelevamientos < ActiveRecord::Migration[5.1]
  def change
    create_table :relevamientos do |t|
      # http://openstreetmapdata.com/info/projections
      t.geometry :coordenadas, srid: 3857
      t.string   :direccion
      t.text     :descripcion
      t.string   :informacion_de_contacto

      t.index :coordenadas, using: :gist
    end
  end
end
