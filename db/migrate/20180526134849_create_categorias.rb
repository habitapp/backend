class CreateCategorias < ActiveRecord::Migration[5.1]
  def change
    create_table :categorias do |t|
      t.string :nombre
      t.belongs_to :madre, index: true
    end
  end
end
