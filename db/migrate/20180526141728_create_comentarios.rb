class CreateComentarios < ActiveRecord::Migration[5.1]
  def change
    create_table :comentarios do |t|
      t.belongs_to :relevamento, index: true
      t.string     :contenido
    end
  end
end
