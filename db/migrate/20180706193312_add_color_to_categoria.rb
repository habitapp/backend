class AddColorToCategoria < ActiveRecord::Migration[5.1]
  def change
    add_column :categorias, :color, :string
  end
end
