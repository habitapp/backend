class CorregirSubcategorias < ActiveRecord::Migration[5.1]
  def up
    # Encontrar todos los relevamientos cuyos campos dinámicos estén
    # asociados a otra categoría
    Relevamiento.all.find_each do |r|
      r.extras.find_each do |e|
        next unless r.categoria_id != e.plantilla.categoria_id
        # Eliminar solo los que tienen valores vacíos para no eliminar
        # datos
        e.destroy if e.valor.empty?
      end
    end
  end

  def down
  end
end
