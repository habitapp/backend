class AddCounterCacheToComentarios < ActiveRecord::Migration[5.1]
  def up
    add_column :relevamientos, :comentarios_count, :integer

    Relevamiento.find_each do |r|
      Relevamiento.update_counters r.id, comentarios_count: r.comentarios.count
    end
  end

  def down
    remove_column :relevamientos, :comentarios_count
  end
end
