class CreateGrupos < ActiveRecord::Migration[5.1]
  def change
    create_table :grupos do |t|
      t.timestamps
      t.string :nombre, unique: true
    end

    create_table :administradoras_grupos do |t|
      t.belongs_to :administradora, index: true
      t.belongs_to :grupo, index: true
    end

    create_table :categorias_grupos do |t|
      t.belongs_to :categoria, index: true
      t.belongs_to :grupo, index: true
    end
  end
end
