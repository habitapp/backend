class AddPrivadoToComentarios < ActiveRecord::Migration[5.1]
  def change
    add_column :comentarios, :privado, :boolean
  end
end
