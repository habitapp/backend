# frozen_string_literal: true

class DeviseCreateAdministradoras < ActiveRecord::Migration[5.1]
  def change
    create_table :administradoras do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      t.timestamps null: false
    end

    add_index :administradoras, :email,                unique: true
    add_index :administradoras, :reset_password_token, unique: true
    add_index :administradoras, :unlock_token,         unique: true
  end
end
