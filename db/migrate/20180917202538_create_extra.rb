class CreateExtra < ActiveRecord::Migration[5.1]
  def change
    create_table :extras do |t|
      t.timestamps
      t.belongs_to :relevamiento, index: true
      t.string :nombre
      t.string :valor
    end
  end
end
