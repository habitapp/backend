class ExtraBelongsToInstancia < ActiveRecord::Migration[5.1]
  def up
    add_belongs_to :extras, :instancia, index: true

    Extra.find_each do |extra|
      extra.update_attribute :instancia_id, extra.relevamiento.instancia_id
    end
  end

  def down
    remove_belongs_to :extras, :instancia
  end
end
