class ActualizarArboles < ActiveRecord::Migration[5.1]
  def up
    # Cambiar el nombre temporalmente para arreglar todos
    Categoria.where(raiz_id: nil).find_each do |c|
      n = c.nombre.dup
      c.update_attribute :nombre, n + "  "
      c.update_attribute :nombre, n
    end
  end

  def down
  end
end
