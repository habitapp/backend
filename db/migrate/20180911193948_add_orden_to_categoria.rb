class AddOrdenToCategoria < ActiveRecord::Migration[5.1]
  def up
    add_column :categorias, :orden, :float

    # Recorrer todas las categorías raíz y asignarles un número entero.
    # Las subcategorías tienen un decimal dentro de su categoría raíz
    Categoria.where(raiz_id: nil).order(:arbol).each_with_index do |c,i|
      o = (i+1).to_f
      c.update_attribute :orden, o
      c.sub_categorias.order(:arbol).each_with_index do |s, j|
        s.update_attribute :orden, o + ((j+1).to_f/Categoria::SUB_BASE)
      end
    end
  end

  def down
    remove_column :categorias, :orden
  end
end
