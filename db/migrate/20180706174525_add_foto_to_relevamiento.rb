class AddFotoToRelevamiento < ActiveRecord::Migration[5.1]
  def change
    add_column :relevamientos, :fotos, :json
  end
end
