class RemoveNombreFromExtras < ActiveRecord::Migration[5.1]
  def change
    remove_column :extras, :nombre, :string
  end
end
