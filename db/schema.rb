# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190610154233) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"
  enable_extension "hstore"

  create_table "administradoras", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "super"
    t.index ["email"], name: "index_administradoras_on_email", unique: true
    t.index ["reset_password_token"], name: "index_administradoras_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_administradoras_on_unlock_token", unique: true
  end

  create_table "administradoras_grupos", force: :cascade do |t|
    t.bigint "administradora_id"
    t.bigint "grupo_id"
    t.index ["administradora_id"], name: "index_administradoras_grupos_on_administradora_id"
    t.index ["grupo_id"], name: "index_administradoras_grupos_on_grupo_id"
  end

  create_table "categorias", force: :cascade do |t|
    t.string "nombre"
    t.bigint "raiz_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "relevamientos_count", default: 0
    t.string "color"
    t.text "descripcion"
    t.string "arbol"
    t.boolean "confidencial", default: false
    t.float "orden"
    t.index ["raiz_id"], name: "index_categorias_on_raiz_id"
  end

  create_table "categorias_grupos", force: :cascade do |t|
    t.bigint "categoria_id"
    t.bigint "grupo_id"
    t.index ["categoria_id"], name: "index_categorias_grupos_on_categoria_id"
    t.index ["grupo_id"], name: "index_categorias_grupos_on_grupo_id"
  end

  create_table "comentarios", force: :cascade do |t|
    t.bigint "relevamiento_id"
    t.string "contenido"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "privado"
    t.string "comentarista_type"
    t.bigint "comentarista_id"
    t.index ["comentarista_type", "comentarista_id"], name: "index_comentarios_on_comentarista_type_and_comentarista_id"
    t.index ["relevamiento_id"], name: "index_comentarios_on_relevamiento_id"
  end

  create_table "extras", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "relevamiento_id"
    t.string "valor"
    t.jsonb "log_data"
    t.bigint "instancia_id"
    t.boolean "required"
    t.json "fotos"
    t.bigint "plantilla_id"
    t.index ["instancia_id"], name: "index_extras_on_instancia_id"
    t.index ["plantilla_id"], name: "index_extras_on_plantilla_id"
    t.index ["relevamiento_id"], name: "index_extras_on_relevamiento_id"
  end

  create_table "grupos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nombre"
  end

  create_table "instancias", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "identifier"
    t.integer "relevamientos_count"
    t.integer "comentarios_count"
    t.index ["identifier"], name: "index_instancias_on_identifier"
  end

  create_table "plantillas", force: :cascade do |t|
    t.bigint "categoria_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "label"
    t.string "value"
    t.string "help"
    t.boolean "photo"
    t.boolean "required"
    t.integer "order"
    t.jsonb "possible_values"
    t.boolean "multiple", default: false
    t.index ["categoria_id"], name: "index_plantillas_on_categoria_id"
  end

  create_table "relevamientos", force: :cascade do |t|
    t.string "direccion"
    t.text "descripcion"
    t.string "informacion_de_contacto"
    t.boolean "confirmado", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "categoria_id"
    t.geography "coordenadas", limit: {:srid=>4326, :type=>"geometry", :geographic=>true}
    t.json "fotos"
    t.integer "comentarios_count"
    t.integer "instancia_id"
    t.jsonb "log_data"
    t.index ["coordenadas"], name: "index_relevamientos_on_coordenadas", using: :gist
  end

  create_table "subscripciones", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
  end

end
