# Crear la primera administradora
unless Administradora.find_by(email: 'fauno@lainventoria.com.ar')
  Administradora.create email: 'fauno@lainventoria.com.ar',
    password: ENV['DEFAULT_ADMIN_PASSWORD'],
    super: true
end

if Rails.env == 'development'
  Relevamiento.destroy_all
  categoria = Categoria.find_or_create_by! nombre: 'Bibliotecas'
  instancia = Instancia.create password: '12345678'

  csv = SmarterCSV.process File.join(Rails.root, 'db', 'bibliotecas.csv'),
    col_sep: ';'

  csv.each do |b|
    r = Relevamiento.find_or_create_by descripcion: b[:biblioteca]
    r.update_attributes descripcion: b[:biblioteca],
      direccion: b[:direccion_normalizada],
      coordenadas: "POINT(#{b[:lng]} #{b[:lat]})",
      categoria: categoria,
      instancia: instancia
  end
end
